// +build !windows

package app_test

import (
	"os"
	"testing"

	"github.com/sirupsen/logrus"
	testlog "github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/release-cli/internal/app"
	"gitlab.com/gitlab-org/release-cli/internal/testdata"
	"gitlab.com/gitlab-org/release-cli/internal/testhelpers"
)

func TestHTTPSCustomCA_Unix(t *testing.T) {
	chdirSet := false

	chdir := testhelpers.ChdirInPath(t, "../", &chdirSet)
	defer chdir()

	s := newUnstartedTLSServer(t, "testdata/certs/localhost.pem", "testdata/certs/localhost.key", handler(t, testdata.ResponseSuccess))

	s.StartTLS()
	defer s.Close()

	tests := []struct {
		name           string
		certFlags      []string
		env            string
		wantErrStr     string
		wantLogEntries []string
	}{
		{
			name:       "with_invalid_path_to_file",
			certFlags:  []string{"--additional-ca-cert-bundle", "unknown/CA.pem"},
			wantErrStr: "no such file or directory",
		},
		{
			name: "with_symlink_outside_of_context_is_invalid",
			// ln -s /etc/ssl/cert.pem symlink_outside.pem
			certFlags:  []string{"--additional-ca-cert-bundle", "testdata/certs/symlink_outside.pem"},
			wantErrStr: "internal/etc/ssl/cert.pem: no such file or directory",
		},
		{
			name: "with_correct_symlink_is_valid",
			// ln -s CA.pem symlink.pem
			certFlags:      []string{"--additional-ca-cert-bundle", "testdata/certs/symlink.pem"},
			wantLogEntries: []string{"Creating Release...", "release created successfully!"},
		},
		{
			name:           "path_traversal",
			certFlags:      []string{"--additional-ca-cert-bundle", "../testdata/certs/CA.pem"},
			wantLogEntries: []string{"Creating Release...", "release created successfully!"},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			log, hook := testlog.NewNullLogger()

			err := os.Setenv("ADDITIONAL_CA_CERT_BUNDLE", tt.env)
			require.NoError(t, err)

			testApp := app.New(logrus.NewEntry(log), t.Name())
			args := []string{"release-cli", "--server-url", s.URL, "--job-token",
				"token", "--project-id", "projectID"}

			args = append(args, tt.certFlags...)

			args = append(args, "create", "--name", "release name", "--description",
				"release description", "--tag-name", "v1.1.0")

			err = testApp.Run(args)
			if tt.wantErrStr != "" {
				require.Error(t, err)
				require.Contains(t, err.Error(), tt.wantErrStr)
			} else {
				require.NoError(t, err)
			}

			require.ElementsMatch(t, toStrSlice(t, hook.AllEntries()), tt.wantLogEntries)
		})
	}
}
