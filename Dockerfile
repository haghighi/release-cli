FROM golang:1.16.3-alpine3.13 AS builder
RUN apk --no-cache add ca-certificates make git && update-ca-certificates

COPY . /release-cli
WORKDIR /release-cli

RUN make build

FROM alpine:3.13

COPY --from=builder /release-cli/bin/release-cli /usr/local/bin/release-cli
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
